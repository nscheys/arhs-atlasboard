var snmp = require('snmp-native');

/**
	SNMP Module for analysing the state of the hosts (cpu load, memory usage, hard disk space)
*/

/*
	Constructor.
	
	@param hostsList  - a list of hosts with their name and ip
	@param job_config - configuration parameters of the job
	@param job_callback - callback of the job
*/
function Snmp(hostsList, job_config, job_callback) {
	this.hostsList = hostsList;
	this.job_config = job_config;
	this.hostsState = []; //results of the snmp analysis for all the hosts
	this.job_callback = job_callback;
}

/*
	Gets the state (mmeory, cpu, hard disk space) of all the hosts.
*/
Snmp.prototype.getAllHostsState = function (cb) {
	var that = this;	
	that.hostsList.forEach(function(host) {
	  that.getHostState(host, function(result){
		
		that.hostsState.push(result);
		
		// wait until all hosts have been analyzed
		if(that.hostsState.length == that.hostsList.length) {
			that.job_callback(null, {job_config : that.job_config , hostsState : that.hostsState});
		}
	  })
	});
			
};

/*
	Gets the state (mmeory,cpu, hard disk space) of a host.
	@param host the host to be analyzed
	@param cb the callback
*/
Snmp.prototype.getHostState = function (host,cb) {
	var that = this;
	
	// create the SNMP session
	var session = new snmp.Session({ host: host.ip, port: 161, community: 'Public',timeouts:[ 1000, 2000, 3000] });
	
	var hostState = {}; // object that contains the state of the host
	hostState.name = host.name;
	
	// Analyse first the processor load
	that.executeProcessorLoadRequest(session, host, hostState, cb);
};


/*
	Fetch the  processor load of the host and execute the next request (i.e. memory usage)
	
	@param host the host to be analyzed
	@param session the SNMP session
	@param hostState  the current host state
	@param cb the callback
*/
Snmp.prototype.executeProcessorLoadRequest = function (session, host, hostState, cb) {
	var that = this;
	
	// The average, over the last minute, of the percentage of time that this processor was not idle.
	session.getSubtree({ oid: [1, 3, 6, 1, 2, 1, 25, 3, 3, 1, 2] }, function (error, varbinds) {
		
		if (error) {
			hostState.processorLoad = 'off' ;
		} else {
			var total = 0;
			
			// sum load for each processor
			for(var i = 0 ; i < varbinds.length ; i++){
				total += varbinds[i].value;
			}			
			// average
			var processorLoad = total / varbinds.length;			
			// update host state
			hostState.processorLoad = processorLoad ;
		}		
		// execute memory usage request
		that.executeMemoryUsageRequest(session,host,hostState,cb);
		
	});
	
};

/*
	Fetch the memory and disk usage of the host 
	
	@param host the host to be analyzed
	@param session the SNMP session
	@param hostState  the current host state
	@param cb the callback
*/
Snmp.prototype.executeMemoryUsageRequest = function (session, host, hostState, cb) {
	var that = this;
	
	// Get the indexes of all logical storage areas contained by the host
	session.getSubtree({ oid: [1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 1] }, function (error, varbinds) {
		
		if (error) {
			hostState.memoryUsage = 'off' ;
			hostState.diskRemainingSpace = 'off' ;
			cb(hostState);			
		} else {
			
			var  nbAreasScanned = 0;
			
			// for each storage area found
			for(var i = 0 ; i < varbinds.length ; i++){
									
					var indexOid = varbinds[i].oid;
					// number that identifies the storage area = last number of oid
					var instance = indexOid[indexOid.length - 1]; 
					var nbStorageAreas = varbinds.length;
					var oids = [ 
									[1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 2, instance], // type of the storage area
									[1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 4, instance], // size of allocation unit (in bytes)
									[1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 5, instance], // size of storage area (nb. allocation units)
									[1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 6, instance]  // storage used (nb. allocation units)
								];
			
					session.getAll({ oids: oids }, function (error, varbinds) {
				
						if(error || !varbinds || varbinds.length != 4){
							hostState.memoryUsage = "off";
							hostState.diskRemainingSpace = "off";
							cb(hostState);
							
						} else{
							var typeOid = varbinds[0].value;
							var allocationUnit = varbinds[1].value;
							var storageSize = varbinds[2].value;
							var storageUsed = varbinds[3].value;							
							var isStorageRAM = (typeOid[typeOid.length - 1] == 2); // RAM = type 2
							var isStorageHD = (typeOid[typeOid.length - 1] == 4); // Hard disk = type 4
																
							if(isStorageHD){
							
								// Compute remaining space on the disk (in gb)
								var remainingGb = ((storageSize - storageUsed)*allocationUnit)/(1024*1024*1024);
								
								if(!hostState.diskRemainingSpace){
									hostState.diskRemainingSpace = [];
								}
								// update state of host
								hostState.diskRemainingSpace.push(remainingGb);
							}	
							
							if(isStorageRAM){
								// Compute percentage of RAM used
								var percentUsage = storageSize !=0 ? ((storageUsed/storageSize)*100) : 0;
								
								// update host state
								hostState.memoryUsage = percentUsage;
							}							
							
							nbAreasScanned++;
					
						    // Execute the callback function only when all storage areas have been analysed
							if(nbAreasScanned == nbStorageAreas){
								cb(hostState);
							}					
						}
					});
			}										
		}
	});
	
};


module.exports.SNMP = Snmp;