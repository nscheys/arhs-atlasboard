widget = {

  onData: function(el, data) {

    //change the widget title if custom data is sent
    if (data.title) {
      $('.widget-title', el).text(data.title);
    }
	
	// display job name
	$('.job-name', el).html(data[0]);	
	var limitFailed = data[1];
	var resultTests = data[2];
	
	// IF BUILD NOT FAILED
	if(limitFailed && resultTests){
		// display progress bar successful tests
		var nbFails = resultTests.failCount;
		var nbSkips = resultTests.skipCount;
		var total = resultTests.totalCount;
		
		var pctSuccess = ((total-nbFails)/total)*100;
		var nbPassed = total-nbFails;
		var result = nbPassed+'/'+total+' PASSED';
		$('.test-results', el).html(result);
		if(nbPassed === total){
			$('.test-results', el).css('color','green');
		} else{
			$('.test-results', el).css('color','red');
		}	
		$('.progress-bar-success').css('width', pctSuccess+'%');
		
		
		// display list of failed tests	
		var failedTests = getFailedTests(resultTests,limitFailed);
		$('.failed-tests',el).empty();
		if(failedTests && failedTests.length > 0){
			$('.failed-tests',el).append('<ul></ul>');
			$('.failed-tests > ul', el).append('<div> FAILURES <div>');
		}
		
		failedTests.forEach(function(failedTest){
			$('.failed-tests > ul', el).append('<li><div>'+failedTest+'</div></li>');			
		});
		
		if(nbFails > limitFailed){
			var nbOtherFailed = nbFails - limitFailed;
			var message =  '+ '+nbOtherFailed+' OTHER '+ (nbOtherFailed > 1 ? 'TESTS' : 'TEST')+' FAILED';
			$('.failed-tests > ul', el).append('<li><div>'+message+'</div></li>');
		}
		
	} else{
		// IF BUILD FAILED
		$('.test-results', el).html("BUILD FAILED");
		$('.test-results', el).css('color','red');
		$('.progress-bar-success').css('width', '0%');	
	}
	
	
	
	
	
	
	/*
		Get the failed tests. Limit the number of results to be displayed.
		@param results - the tests results JSON
		@param limit - the limit
	*/	
	function getFailedTests(results,limit){
		var failedTests = [];
		
		var childReports = results.childReports;
		childReports.forEach(function(childReport) {
			var suites = childReport.result.suites;
			if(suites){
				suites.forEach(function(suite){
					var cases = suite.cases;
					if(cases){
						cases.forEach(function(testCase){
							if(failedTests.length < limit){
								var pathToClassName = testCase.className;
								var className= pathToClassName.substring(pathToClassName.lastIndexOf(".")+1,pathToClassName.length);
								var testCaseName = className+"."+testCase.name;					  
								if(testCase.status == "FAILED" || testCase.status == "REGRESSION" ){
									failedTests.push(testCaseName);								
								}	
							}							
						});
					}
				});
			}			
		});		
		return failedTests;
	};
	
	
	}
};

