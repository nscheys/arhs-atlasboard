widget = {

  onData: function(el, data) {
  
	if(data){
		
		// Sort the jobs alphabetically
		data.sort(function(h1, h2){return h1.displayName.localeCompare(h2.displayName);});
	  
		var buildResults = parseBuildData(data);
				
		
		$(el).find('.builds').highcharts({
			chart: {
				type: 'bar',
				width: null,
				height: el.parent().height()-10
			},
			title: {
				text: ""
			},
			xAxis: [{
				categories: buildResults.jobs ,
				gridLineWidth: 0,
				type: 'category',
				lineWidth:0,
				minorGridLineWidth: 0,
				lineColor: 'transparent',
				minorTickLength: 0,
				tickLength: 0,
				labels: {
					style: {
						fontSize:'20px'
					}
				},
			}, { 
				categories: buildResults.summaries,
				type: 'category',
				lineWidth:0,
				minorTickLength: 0,
				tickLength: 0,
				lineColor: 'transparent',
				labels: {
					style: {
						fontSize:'20px'
					}
				},
				opposite: true
			}],
			
			yAxis: {
				min: 0,
				gridLineWidth: 0,
				minorGridLineWidth: 0,
				minorTickLength: 0,
				tickLength: 0,
				allowDecimals : false,
				title: {
					text: ''
				},
				labels: {
					enabled: false,
					style: {
						fontSize:'20px'
					}
				},
				
			},
			legend: {
				enabled: false
			},
			plotOptions: {
				
				bar: {
					stacking: 'percent',
					borderWidth : 0,
					//borderRadius: 15,
					dataLabels: {
						enabled: false,
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
						style: {
							fontWeight: 'bold',
							fontSize : '20px',				
						},
						
					}
				},
				
				
			},
			series: [ {
				name: 'Skipped',
				maxPointWidth: 30,
				xAxis : 1,
				data: buildResults.results.skipped,
				color : '#bbb'
			}, {
				name: 'Failed',
				maxPointWidth: 30,
				data: buildResults.results.failed,
				color : 'red',
			}, {
				name: 'Passed',
				maxPointWidth: 30,
				xAxis : 1,
				data: buildResults.results.passed,
				color : 'green',
			}, {
				name: 'build remaining time',
				maxPointWidth: 30,
				xAxis : 1,
				data: buildResults.results.estimatedRemainingTime,
				color : 'gray',
			}, {
				name: 'build progress',
				maxPointWidth: 30,
				xAxis : 1,
				data: buildResults.results.progress,
				color : 'orange',
			}]
		});
		
	}
	
		
	
	function initBuildResult(){
	
		var buildResults = {};
		buildResults.failed = [];
		buildResults.passed = [];
		buildResults.progress = [];
		buildResults.estimatedRemainingTime = [];
		buildResults.skipped = [];	
		return buildResults;		
	}
	
	function pushBuildResult(buildResults,nbFailed,nbPassed,nbSkipped,progress, buildRemainingTime){
		
		if(buildResults.failed && buildResults.passed && buildResults.progress 
			&& buildResults.skipped && buildResults.estimatedRemainingTime){
		
			buildResults.failed.push(nbFailed);
			buildResults.passed.push(nbPassed);
			buildResults.skipped.push(nbSkipped);
			buildResults.progress.push(progress);
			buildResults.estimatedRemainingTime.push(buildRemainingTime);
				
		}	
	}
	
	function parseBuildData(data){
	
		var jobs = [];
		var summaries = [];
		var buildResults = initBuildResult();
		data.forEach(function(build){
			var jobName = build.result ? ('<span style="color:'+getBuildColor(build)+';">'+build.displayName+'</span>') : build.displayName ;
			var summary = getBuildSummary(build);
			summaries.push(summary);
			jobs.push(jobName);
			if(build.result == 'FAILURE'){
				pushBuildResult(buildResults,1,0,0,0,0);							
			} else if(build.isBuilding) {	
				var buildProgress = getBuildProgress(build);
				pushBuildResult(buildResults,0,0,0,buildProgress,100-buildProgress);
			} else {
				pushBuildResult(buildResults,build.testResults.failCount,build.testResults.passedCount,build.testResults.skipCount,0,0);					
			}
			
		});		
		return {jobs : jobs, results : buildResults, summaries : summaries} ;
	}
	
	function getBuildProgress(build){
		if(build.isBuilding) {
			var estimationTime = build.buildEstimationTime;
			var buildStartTime = build.buildStartTime;
			var currentTime = new Date().getTime();
			var elapsedBuildTime = currentTime - buildStartTime;
			var buildProgress = elapsedBuildTime > estimationTime ? estimationTime : elapsedBuildTime;
			 
			return Math.round((buildProgress/estimationTime)*100); 
		}
		
		return 100;
	}
	
	
	
	function getBuildColor(build){
		switch(build.result) {
		case "FAILURE":
			return "red";
		case "SUCCESS":		
			return build.isBuilding ? "white" : "green";
		case "UNSTABLE":
			return "orange";
		default:
			return "white";
		} 
	}
	
	function getBuildSummary(build){
		var buildSummary = '<span style="color:'+getBuildColor(build)+';">'
		if(build.result == 'FAILURE'){
			buildSummary += 'FAILED';					
			} else if(build.isBuilding) {
				var buildProgress = getBuildProgress(build);
				buildSummary += 'IN PROGRESS ('+buildProgress+'%)';
			} else {
				buildSummary += (build.testResults.passedCount+'/'+build.testResults.totalCount);					
			}
			
		buildSummary += '</span>';
		return buildSummary;
	}
		
	
	}
};

