widget = {
	onData: function(el, data) {
    
		var warningThreshold = data.warningThreshold;
		var criticalThreshold = data.criticalThreshold;
		var lastResult = data.issues.lastResult;
		var fromResult = data.issues.fromResult;
		
		if(lastResult && fromResult){
			
			var lastResultParsed = parseSonarResults(lastResult.v);
			var fromResultParsed = parseSonarResults(fromResult.v);
			
			// Display dates of sonar analysis
			var options = {year: "2-digit", month: "2-digit", day: "2-digit"};
			var fromDate = new Date(fromResult.d).toLocaleDateString("en-GB",options);
			var toDate = new Date(lastResult.d).toLocaleDateString("en-GB",options);		
			$('#dates', el).html("("+toDate+" \u2190 "+ fromDate+")");
			
			if(lastResultParsed && fromResultParsed ){
				
				// Display nb issues and compliance
				for(var fieldType in lastResultParsed){
					setField(lastResultParsed[fieldType],fromResultParsed[fieldType],fieldType, $('#'+fieldType, el));
				}			
			}				
			
		}
		
			
		function parseSonarResults(data){
			if(data.length != 7){
				return null;
			}
			return {ruleCompliance : data[0], nbIssues:data[1],blockerIssues:data[2],criticalIssues:data[3],majorIssues:data[4],minorIssues:data[5],infoIssues:data[6]};			
		};
		
		function setField(toValue, fromValue, fieldType, htmlElement){
			
			var delta = toValue - fromValue;
			var fieldValue = toValue;
			
			if(fieldType == "ruleCompliance"){
				if(toValue <= criticalThreshold ){
					fieldValue = "<span class='critical'>"+toValue+"%</span>";
				} else if(toValue > criticalThreshold && toValue <= warningThreshold){
					fieldValue = "<span class='warning'>"+toValue+"%</span>";
				} else{
					fieldValue += "%";
				}										
			}
		
			if(delta===0){
				fieldValue += " (=) ";
			} else if(delta > 0){
				fieldValue += "<span class='plus'> (+"+delta+") </span> ";
			} else{
				fieldValue += "<span class='minus'> ("+delta+") </span> ";
			}
			
			htmlElement.html(fieldValue);
			
		}
	}
};

