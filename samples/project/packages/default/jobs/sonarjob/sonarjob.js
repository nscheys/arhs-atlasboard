/*
	Sonar Job
 */

var request = require('request');
var URLParser = require('../../service/urlparser').URLParser;

module.exports = function(config, dependencies, job_callback) {
	
	var urlObject = new URLParser(config.sonarUrl,'/sonar/api/timemachine?resource='+config.projectKey
		+'&metrics=violations_density,violations,blocker_violations,critical_violations,major_violations,minor_violations,info_violations');
	var options = {
		url : urlObject.getNormalizedUrl(),
		
		auth: {
				'user': config.user,
				'pass': config.password				
			}
	};

	request.get(options, function(err, res, body) {	
			
		if (err){ 
			var url = "<a href='"+config.sonarUrl+"'>"+config.sonarUrl+"</a>";	
			return job_callback("Could not find Sonar instance at "+ url); 
		}
		
		if (!err && res.statusCode == 200) {
					
			var violations_result = JSON.parse(body);
			var violations_cells = violations_result[0].cells;
			var last_analysis = violations_cells[violations_cells.length-1];
			var last_analysis_date = last_analysis.d;
			
			var data = {};
			data.lastResult = last_analysis;
			
			for(var i = 0 ; i < violations_cells.length ; i++ ){
				
				var violation_result = violations_result[0].cells[i];
				var analysis_date = violation_result.d;
				var fromDate = subtractDays(last_analysis_date, config.comparisonInterval); 			
				if(getTimestampFromIsoString(analysis_date) >= getTimestampFromIsoString(fromDate)){
					data.fromResult = violation_result;
					break;
				}
			}
			
			job_callback(null,{warningThreshold : config.complianceWarningThreshold, criticalThreshold : config.complianceCriticalThreshold, issues : data});
		}  else {
			if(res.statusCode == 404){
				try {
					var errorJson = JSON.parse(res.body);
					job_callback("Error " + res.statusCode + ": "+ errorJson.err_msg);
				} catch(e) {
					job_callback("Error " + res.statusCode + ": Incorrect Sonar url");
				}

	
			} else if(res.statusCode == 401){
				job_callback("Error " + res.statusCode + ": Not authorized - Check your credentials");
			} else{
				job_callback("Error Sonar " + res.statusCode);
			}						
			
		}
    });
	
};

function getTimestampFromIsoString(isoString){
	return new Date(isoString).getTime();
}

function subtractDays(date,nbDaysToSubtract){

	var subtractedDate = new Date(date);
	if(nbDaysToSubtract){
		subtractedDate.setDate(subtractedDate.getDate()-nbDaysToSubtract);
	}
	return subtractedDate.toISOString();
}










