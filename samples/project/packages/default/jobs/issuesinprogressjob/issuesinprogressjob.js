/*
  Issues in progress job.
 */

var request = require('request');
var URLParser = require('../../service/urlparser').URLParser;

module.exports = function(config, dependencies, job_callback) {

	var urlObject = new URLParser(config.jiraUrl,"/rest/greenhopper/1.0/xboard/work/allData.json?rapidViewId="+config.rapidViewId);
	var options = {
		url : urlObject.getNormalizedUrl('https'),
		auth: {
				'user': config.user,
				'pass': config.password				
			}
	};
	
	request.get(options, function(err, res, body) {	
	
		var url = "<a href='"+options.url+"'>"+options.url+"</a>";
		
		if (err){ return job_callback("Could not find Jira at "+ url); }
		if (!err && res.statusCode == 200) {		
			var sprint = JSON.parse(body);
			if(sprint.sprintsData.sprints.length > 0){
				var sprintName=	sprint.sprintsData.sprints[0].name;
				var inprogress = [];
				var issues = sprint.issuesData.issues;
					
					issues.forEach(function(issue){
						if(isIssueInProgress(issue)){
							inprogress.push({
								"key" : issue.key,
								"summary" : issue.summary,
								"avatarUrl" : issue.avatarUrl,
								"estimate" : issue.estimateStatistic.statFieldValue,
								"assignee" : issue.assignee,
								"typeUrl" : issue.typeUrl,
								"priorityName" : issue.priorityName
							})
						}
					});
				job_callback(null, {sprintName : sprintName , inprogressIssues : inprogress});			
			} else{
				job_callback("No sprints defined for the view "+config.rapidViewId)			
			}
			
			
		} else {
			if(res.statusCode == 404 || res.statusCode == 500 ){
				job_callback("Error " + res.statusCode + ": "+ JSON.parse(res.body).errorMessages);
			} else if(res.statusCode == 403 || res.statusCode == 401 ){
				var loginUrl = new URLParser(config.jiraUrl).getNormalizedUrl('https')+"/login.jsp";
				var loginUrlLink = "<a href='"+loginUrl+"'>"+loginUrl+"</a>";
				job_callback("Error " + res.statusCode + ": Check your credentials. If the problem persists, try to login manually via " + loginUrlLink);
			} else{
				job_callback("Error JIRA " + res.statusCode);
			}						
			
		}
    });
	
};

function isIssueInProgress(issue){
	var inprogressStatuses = ["3", "10014", "10015", "10008","10013","10010"];
	if(inprogressStatuses.indexOf(issue.statusId) != -1){
		return true;
	}
	return false;
} 