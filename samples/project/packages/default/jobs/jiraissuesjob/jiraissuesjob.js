/*

  JIRA Issues job.


 */

var request = require('request');
var JiraApi = require('jira').JiraApi;
var URLParser = require('../../service/urlparser').URLParser;


module.exports = function(config, dependencies, job_callback) {

  var urlObject = new URLParser(config.jira_server);
  
  var jira = new JiraApi('https', urlObject.getHostWithoutProtocol() , "", config.user, config.password, '2');
	

   jira.searchJira(config.jql,["summary","status","assignee","description","reporter","issuetype","priority"],function(error, data) {
      
		if(error){
			return  error.message ? job_callback("Could not conneec to jira : Check the url of your jira server") : job_callback(error) ;
		}
		
		return data ? job_callback(null,data) : job_callback("Could not get issues from Jira. The problem could come from your access rights. Check your credentials.");
			    	  
  });


};