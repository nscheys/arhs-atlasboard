/*
	VM State Job
 */

var fs = require('fs');
var csv = require('csv');
var SNMP = require('../../service/snmp').SNMP;

module.exports = function(config, dependencies, job_callback) {

	
	if(config.vms && config.vms.length > 0){
		
		var hostsList = [];
		config.vms.forEach(function(vm){		
				// Fill list of hosts
				if(vm.name && vm.host){
					hostsList.push({name:vm.name,ip:vm.host});
				}
		});
		// Get information about the VM state and send the results to the view.
		var snmp = new SNMP(hostsList,{maxCpu:config.maxCpu, maxRam:config.maxRam, minDiskGB:config.minDiskGB},job_callback);	
		snmp.getAllHostsState();
	} else{
		return job_callback("No VM defined in the parameters");
	}

	
};








