/*
 *	Burndown chart Job
 */

var request = require('request');
var URLParser = require('../../service/urlparser').URLParser;

module.exports = function(config, dependencies, job_callback) {

	var urlObject = new URLParser(config.jiraUrl,"/rest/greenhopper/1.0/rapid/charts/scopechangeburndownchart.json?rapidViewId="+config.rapidViewId+"&sprintId="+config.sprintId);
	var options = {
		url : urlObject.getNormalizedUrl('https'),
		auth: {
				'user': config.user,
				'pass': config.password				
			}
	};
	
	request.get(options, function(err, res, body) {
	
		var url = "<a href='"+options.url+"'>"+options.url+"</a>";
		
		if (err){ return job_callback("Could not find Jira at "+ url); }
		if (!err && res.statusCode == 200) {		
			var sprintScope = JSON.parse(body);
			var burndownData = getBurndownData(sprintScope);
			var guidelineData = getGuideline(sprintScope,burndownData[0][1]);
			job_callback(null,[burndownData,guidelineData]);
			
		} else {
			if(res.statusCode == 404 || res.statusCode == 500 ){			
				job_callback("Error " + res.statusCode + ": "+ JSON.parse(res.body).errorMessages);
			} else if(res.statusCode == 403 || res.statusCode == 401 ){
				var loginUrl = new URLParser(config.jiraUrl).getNormalizedUrl('https')+"/login.jsp";
				var loginUrlLink = "<a href='"+loginUrl+"'>"+loginUrl+"</a>";
				job_callback("Error " + res.statusCode + ": Check your credentials. If the problem persists, try to login manually via " + loginUrlLink);
			} else{			
				job_callback("Error " + res.statusCode + ": "+ JSON.parse(res.body).errorMessages);
			}						
			
		}
				
    });
	
};


function getGuideline(sprintScope,initialEstimation){
	var tab = [];
	var rates = sprintScope.workRateData.rates;
	var sprintEndTime = sprintScope.endTime;
	var total = 0;
	rates.forEach(function(rate){
		if(rate.rate === 1 && rate.start <= sprintEndTime){
			var start = rate.start;
			var endR= rate.end > sprintEndTime ? sprintEndTime : rate.end;
			total += (endR - start);		 
		}
	});
	
	var nbPeople = initialEstimation / (total/1000/3600/3);
	
	var current = initialEstimation;
	rates.forEach(function(rate){
		if(rate.start <= sprintEndTime){
			if(rate.rate === 1){
				var start = rate.start;
				var endR= rate.end > sprintEndTime ? sprintEndTime : rate.end;
				total = (endR - start);
				current -= ((total/1000/3600/3) * nbPeople);
				if(tab.length ===0){
					tab.push([start,initialEstimation]);
				}
				
				tab.push([endR,current]);
			} else {
				var start = rate.start;
				var endR= rate.end > sprintEndTime ? sprintEndTime : rate.end;
				tab.push([endR, current]);
			}
		}
	});
	
	return tab;
}


function getBurndownData(sprintScope){
	var sprintStartTime = sprintScope.startTime;
	var sprintCompleteTime = sprintScope.completeTime;
	var current = 0;
	var data = [];
	var issueCurrentEstimate = {};
	for(var dateString in sprintScope.changes){
		var value = sprintScope.changes[dateString];
		var date = parseInt(dateString);
		var issueKey = value[0].key;
		var estimate = getIssueEstimate(value);
		var currentIssueValue = issueCurrentEstimate[issueKey] ? issueCurrentEstimate[issueKey]: 0 ;
		
		// Compute value for sprint start
		if(date > sprintStartTime && data.length === 0){
			data.push([sprintStartTime, current/3600]);
		}	
			
		// Scope change or issue added to sprint
		if(estimate != null){
			issueCurrentEstimate[issueKey] = estimate;
			var delta = estimate - currentIssueValue;
				
			if(delta != 0){		
				current += delta;
				pushData(data,date,current,sprintStartTime);
			}		       
		}
		
		// Issue resolved or closed
		if (value[0].column && (value[0].column.newStatus == "5" ||  value[0].column.newStatus == "6") ){
			current -= currentIssueValue;
			pushData(data,date,current,sprintStartTime);
		}
		
		// Issue reopened
		if (value[0].column && value[0].column.newStatus == "4" && date >= sprintStartTime){
			current += currentIssueValue;
			pushData(data,date,current,sprintStartTime);
		}
		
		// Issue removed
		if (value[0].added === false){
			current -= currentIssueValue;
			pushData(data,date,current,sprintStartTime);
		}

	}
	
	// add last point
	pushData(data,sprintCompleteTime,current,sprintStartTime);
	return data;
}


function getIssueEstimate(change){
	if(change[0].statC){
		if(change[0].statC.newValue){
			return change[0].statC.newValue;
		}
		// Estimate removed then no new value
		if(change[0].statC.oldValue){
			return 0;
		}
	}
}

function pushData(data,date,currentY,sprintStartTime){
	if(date > sprintStartTime){
		var hour = (currentY/3600);
		data.push([date,hour]);
	}
}





