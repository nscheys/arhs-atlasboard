[![arhs](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/arhs-group.png)](http://www.arhs-group.com/)

Provided by the Arηs group. [http://www.arhs-group.com/](http://www.arhs-group.com/)

![Atlasboard](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/wallboard8x6.png)

[https://bitbucket.org/nscheys/arhs-atlasboard](https://bitbucket.org/nscheys/arhs-atlasboard)

##Requirements##

+ Python 2.7
+ Microsoft Framework 2.0 SDK
+ Node.js

##Installation##

`npm install -g arhs-atlasboard`


##Creating your first project##

After installing arhs-atlasboard as a global module, you can generate a new fully functional project that contains a sample dashboard by doing the following : 

`arhs-atlasboard create`

This will create a directory called arhs-atlasboard in the current directory. Move to this directory and start the atlasboard server:

	cd arhs-atlasboard
	arhs-atlasboard start 

You can now have access to your project's home page via [http://localhost:3000](http://localhost:3000). The project's home page allows you to create new dashboards and displays all the created dashboards.  

You can also directly access the example dashboard via [http://localhost:3000/dashboard](http://localhost:3000/dashboard) (login:"dashboard", password:"password").


##Configuring your dashboard##

There are 2 ways to configure your dashboard (position of widgets, size of widgets, value of parameters, ...): 

+ Modify manually the JSON file containing the dashboard configuration. The configuration file for the sample project can be found at packages/default/dashboards/dashboard.json.
For more information on how to configure the dashboard, [check the wiki of Atlasboard. ](https://bitbucket.org/atlassian/atlasboard/wiki/Dashboards)
+ Use the web interface provided by Arhs Atlasboard that allows to easily add, resize, remove and configure the widgets of your dashboard. See screens below.

### Main screen ###

![dashboardmain](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/dashboardmain.PNG)

### Add widget popup ###

![addwidget](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/addwidget.PNG)

A list of available widgets is displayed along with a screenshot and a description of each widget.

### Configure widget popup ###

![configure](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/config.PNG)

In this popup, you can set the values for the configuration parameters of your widget or remove the widget from your dashboard.

##Creating new jobs and widgets##

Arhs-Atlasboard comes with several widgets that you can reuse for your dashboard. However, following your needs, you may want to create your own widgets.
For that purpose, follow these three steps :

1. Define the configuration parameters of the widget. More on that in the next section.
2. Create the job that will collect the needed data. More information [here](https://bitbucket.org/atlassian/atlasboard/wiki/Jobs). 
3. Create the widget that will render the data provided by the job. More information [here](https://bitbucket.org/atlassian/atlasboard/wiki/Widgets).

###Define widget parameters###

Widgets parameters are defined in a JSON file located at config/atlasboard.json. It contains the following elements:

+ `widgets.<widgetName>` contains the properties and parameters of the widget named <widgetName> :

    + `title`: the widget title
    + `job` : the name of the job corresponding to the widget
	+ `description` : A description of the widget
	+ `width` : Initial width of the widget (nb columns)
	+ `height` : Initial height of the widget (nb rows)
	+ `config` : An object containing the configuration parameters of the widget. For each parameter :
	
		+ `type`: Type of the parameter. Possible values : "int","float","string","array","password"
		+ `help`: Description of the parameter
		+ `defaultValue`: default value of the parameter (optional)
		+ `min` : Minimum value of the parameter (optional)
		+ `max` : Maximum value of the parameter (optional)
		+ `optional` : If true, the parameter is optional.
		+ `fields` : Only for parameters of type "array". Allows to define the name of the fields for the array elements.
		
	 
####Example config file####
	{
	  "live-logging" : {
		"enabled":false
	  },
	  "widgets":{                  
			"vmstate":{
				"title":"VM State",
				"job":"vmstatejob",
				"description":"The widget allows to monitor the state (cpu, memory, disk space) of a list of virtual machines.",
				"width":2,
				"height":2,
				"config":{
					"interval": {"type":"int", "help":"The execution interval in seconds.", "defaultValue":120, "min":1},
					"vms": {"type":"array", "help":"The list of VM to monitor.", "fields": ["name","host"]},												
					"maxCpu": {"type":"float", "help":"The threshold (in %) above which the cpu load is considered critical", "defaultValue":90,"optional":true, "min":0,"max":100},
					"maxRam": {"type":"float","help":"The threshold (in %) above which the ram usage is considered critical", "defaultValue":90,"optional":true, "min":0,"max":100},
					"minDiskGB": {"type":"float","help":"The threshold (in Gb) under which the hard disk space left is considered critical", "defaultValue":1,"optional":true, "min":0}
								
				}
			}                                                             
	  }
	}
	


##Example widgets/jobs##


###Builds overview###
The widget allows to monitor the status of a list of Jenkins build jobs. Only the last build results are shown.

![builds](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/builds.png)

Parameters : 

+ `jenkinsUrl`: The url of the jenkins instance.
+ `jobs`: The list of jobs to monitor.
+ `username`: User name for authentication on Jenkins
+ `apiKey`: API Token for authentication on Jenkins

###Smoke tests###
The widget allows to monitor the status of the smoke tests and displays the failed tests.

![smoke](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/smoke.png)

Parameters : 

+ `jenkinsUrl`: The url of the jenkins instance.
+ `smokeJob`: The job that corresponds to the smoke tests.
+ `limitFailed`: Maximum number of failed tests that are displayed on the screen.
+ `username`: User name for authentication on Jenkins
+ `apiKey`: API Token for authentication on Jenkins

###Sonar###
The widget allows to monitor the sonar issues of a project. It shows the rules compliance, the nummber of issues and their criticality levels.

![sonar](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/sonar.png)

Parameters : 

+ `jenkinsUrl`: The url of the jenkins instance.
+ `sonarUrl`: The url of the sonar instance.
+ `projectKey`: The project key of the monitored project
+ `comparisonInterval`: The period of time used (in days) for comparing results. Ex: If the period is 14 days, the last sonar analysis is compared with the sonar analysis from 2 weeks ago.
+ `complianceWarningThreshold`: The warning threshold (in %) for rules compliance. If the compliance is lower than the threshold, the compliance's value is displayed in orange.
+ `complianceCriticalThreshold`: The critical threshold (in %) for rules compliance. If the compliance is lower than the threshold, the compliance's value is displayed in red.
+ `username`: User name for authentication on Sonar
+ `password`: Password for authentication on Sonar


###Stash###
The widget displays the latest commits on a Git Stash repository.

![stash](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/stash.png)

Parameters : 

+ `stash_server`: The url of the stash server
+ `project`: The project key identifying the project that is monitored.
+ `repo`: The name of the git repository
+ `branch`: Branch identifier (ex: refs/heads/master)
+ `username`: User name for authentication on the repository
+ `password`: Password for authentication on the repository

###Jira Issues###
The widget displays a list of jira issues filtered according to a particular criterion.

![issues](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/issues.png)

Parameters :
 
+ `jira_server`: The url of the jira server.
+ `jql`: The jql expression for filtering the issues (ex : project = <projectName> AND status in (Open))
+ `user`: User name for authentication on Jira
+ `password`: Password for authentication on Jira

###Issues in progress###
The widget shows the JIRA issues that are in progress for a particular sprint.

![issuesinprogress](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/issuesinprogress.png)

Parameters : 

+ `jiraUrl`: The url of the jira instance
+ `rapidViewId`: The rapid view id
+ `user`: User name for authentication on Jira
+ `password`: Password for authentication on Jira

###Releases###
The widget shows information about the next planned release, in particular the remaining days before the release date.

![releases](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/releases.png)

Parameters :
 
+ `jiraUrl`: The url of the jira instance
+ `projectCode`: The project code
+ `warningThreshold`: If the number of days remaining before the deadline is lower than the threshold, it will be displayed in orange.
+ `criticalThreshold`: If the number of days remaining before the deadline is lower than the threshold, it will be displayed in red.
+ `user`: User name for authentication on Jira
+ `password`: Password for authentication on Jira

###Burndown chart###
The widget shows the burndown chart of a particular sprint.

![burndown](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/burndown.png)

Parameters : 

+ `jiraUrl`: The url of the jira instance
+ `rapidViewId`: The rapid view id
+ `sprintId`: The sprint id
+ `user`: User name for authentication on Jira
+ `password`: Password for authentication on Jira

###VM State###
The widget allows to monitor the state (cpu, memory, disk space) of a list of virtual machines.

![vmstate](https://bitbucket.org/nscheys/arhs-atlasboard/raw/master/screenshots/vmstate.png)

Parameters : 

+ `vms`: The list of VM to monitor
+ `maxCpu`: The threshold (in %) above which the cpu load is considered critical
+ `maxRam`: The threshold (in %) above which the ram usage is considered critical
+ `minDiskGB`: he threshold (in Gb) under which the hard disk space left is considered critical


Note that each widget has a configuration parameter called "interval" which defines the refresh rate of the widget.


